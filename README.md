# Netcup-DynDNS

Python script that updates dns records via [Netcup CCP API](https://www.netcup-wiki.de/wiki/DNS_API).
Gets your external IPv4 from https://www.ipify.org/
___

## **Contents**
  - [Installation](#installation)
    - [Steps](#steps)
  - [Configuration](#configuration)
    - [Options](#options)
    - [Example config file](#example-config-file)

<br>

___
## Installation

The installation script moves the script files to /opt/netcup-dyndns, and sets up an systemd timer and service that executes every minute, and changes your configured DNS records if your ip changed. 

### Steps
1. Create a Subdomain in the Netcup CCP for use with this script.
2. Clone repo
    ```
    git clone https://gitlab.com/cruecker/netcup-dyndns.git
    ```
3. cd into directory
   
   ```
   cd netcup-dyndns
   ```
4. Mark installation script executable and run it as root
    ```
    chmod +x && sudo setup/install.sh
    ```
5. Create config file in /opt/netcup-dyndns/ncdyndns.cfg, see [Example config file](#example-config-file)

<br>

___
## Configuration
<br>

### Options
| customernumber | Your netcup customer number                                                                                                  |
|----------------|------------------------------------------------------------------------------------------------------------------------------|
| apikey         | Your netcup API key, see [Netcup Wiki](https://www.netcup-wiki.de/wiki/CCP_API#Beantragung_eines_API-Passworts_und_API-Keys) |
| apipassword    | Your api password, see [Netcup Wiki](https://www.netcup-wiki.de/wiki/CCP_API#Beantragung_eines_API-Passworts_und_API-Keys)   |
| domainname     | Your Domain name, like example.com                                                                                           |
| hostnames      | The subdomain, dynamic, needs to exist before running the script!                                                            |
| debug          | Verbose logging                                                                                                              |
<br>

### Example config file

    [Account]
    apikey = VERYLONGAPIKEY
    customernumber = 000000 
    apipassword = VERYSECUREAPIPASSWORD
    domainname = example.com
    debug = False
    hostnames = dynamic
